package pong;
import javafx.scene.canvas.GraphicsContext;

import static pong.CanvasMainClass.SCREEN_HEIGHT;
import static pong.CanvasMainClass.SCREEN_WIDTH;
import static pong.PlayerBlock.BLOCK_HEIGHT;
import static pong.PlayerBlock.BLOCK_WIDTH;

import java.util.Random;

public class GameBall {
    private double xpos = 0;
    private double ypos = 0;
    protected double xvel = 1;
    protected double yvel = 1;
    public static double BALL_DIAMETER = 10;
    public static double BALL_RADIUS = BALL_DIAMETER / 2;

    public GameBall(double x, double y) {
        this.xpos = x;
        this.ypos = y;
        Random rand = new Random();
        //this.xvel = rand.nextdouble(5) + 1;
        //this.yvel = rand.nextDouble() * 5.0;
        if(rand.nextBoolean()) {
            this.xvel = -this.xvel;
        }
        if(rand.nextBoolean()) {
            this.yvel = -this.yvel;
        }
    }

    public void update(GameLevel level) {
        this.xpos += this.xvel;
        this.ypos += this.yvel;
        //check top and bottom collision.
        if((this.ypos > SCREEN_HEIGHT - BALL_DIAMETER) || (this.ypos <= 0)) {
            this.yvel = -yvel;
        }
        //check right and left collision.
        if((this.xpos >= SCREEN_WIDTH - BALL_DIAMETER) || (this.xpos <= 0)) {

            //Player lost.
            if(this.xpos < SCREEN_WIDTH / 2) {
                level.cpu.score++;
                System.out.println("CPU SCORED.");
            }
            //CPU lost.
            else {
                level.player.score++;
                System.out.println("PLAYER SCORED.");
            }
            //TODO: Reset level and add podouble to round winner.
            this.xvel = -xvel;
            this.yvel = Math.round(new Random().nextDouble() * 5);
            this.xpos = SCREEN_WIDTH / 2 - GameBall.BALL_DIAMETER;
            this.ypos = SCREEN_HEIGHT / 2 - GameBall.BALL_DIAMETER;
            level.pause();
        }
        //Check cpu paddle collision against it's Y position.
        double ballCenterY = this.ypos + BALL_RADIUS;
        if(this.xpos >= SCREEN_WIDTH - BLOCK_WIDTH - BALL_DIAMETER) {
            if(ballCenterY >= level.cpu.getYpos() && ballCenterY <= level.cpu.getYpos() + BLOCK_HEIGHT) {
                System.out.println("CPU COLLISION");
                //this.xvel = -xvel;
                recalculateVelocityOnBounce(level.cpu.getYpos());
                if(xvel > 0) xvel = -xvel;
                xvel--;
                this.xpos = (SCREEN_WIDTH - BLOCK_WIDTH - BALL_DIAMETER) + xvel;

            }

        }
        //Check player paddle collision against it's Y position.
        if(this.xpos <= BLOCK_WIDTH) {
            if(ballCenterY >= level.player.getYpos() && ballCenterY <= level.player.getYpos() + BLOCK_HEIGHT) {
                System.out.println("PLAYER COLLISION");
                //this.xvel = -xvel;
                recalculateVelocityOnBounce(level.player.getYpos());
                if(xvel < 0) xvel = -xvel;
                xvel++;
                this.xpos = BLOCK_WIDTH + xvel;
            }
        }
    }
    private void recalculateVelocityOnBounce(double paddleTopY) {
        //Taken from:
        // https://gamedev.stackexchange.com/questions/4253/in-pong-how-do-you-calculate-the-balls-direction-when-it-bounces-off-the-paddl
        double relativedoubleersectY = (paddleTopY + (BLOCK_HEIGHT / 2)) - this.ypos - BALL_RADIUS;
        double normalizedRelativedoubleersectionY = (relativedoubleersectY / (BLOCK_HEIGHT / 2));
        double bounceAngle = normalizedRelativedoubleersectionY * 45;
        this.xvel = Math.round(6 * Math.cos(bounceAngle));
        this.yvel = Math.round(6 * -Math.sin(bounceAngle));
        //return 0;
    }
    public void render(GraphicsContext gc) {
        //gc.setFill(Color.RED);
        //double centerY = this.ypos + BALL_RADIUS;
        //double centerX = this.xpos + BALL_RADIUS;
        //Draw horizontal line
        //gc.fillRect(this.xpos , centerY - 1, BALL_DIAMETER, 2);
        //gc.fillRect(centerX - 1, this.ypos, 2, BALL_DIAMETER);
        //gc.setFill(Color.WHITE);
    }

    public double getXpos() {
        return xpos;
    }

    public double getYpos() {
        return ypos;
    }

}
