package pong;
import javafx.application.Application;
import javafx.event.EventHandler;
import javafx.geometry.VPos;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.TextAlignment;
import javafx.stage.Stage;
import javafx.scene.Scene;
import javafx.scene.Group;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.FontPosture;
import javafx.scene.image.Image;
import javafx.animation.AnimationTimer;
public class CanvasMainClass extends Application
{
    public static int SCREEN_WIDTH = 320;
    public static int SCREEN_HEIGHT = 240;
    private GameLevel level = new GameLevel();
    public static void main(String[] args)
    {
        launch(args);
    }

    @Override
    public void start(Stage theStage)
    {
        theStage.setTitle( "Canvas Example" );

        Group root = new Group();
        Scene theScene = new Scene( root );
        theStage.setScene( theScene );

        Canvas canvas = new Canvas( SCREEN_WIDTH, SCREEN_HEIGHT );
        root.getChildren().add( canvas );

        GraphicsContext gc = canvas.getGraphicsContext2D();
        //key input handling.
        theScene.setOnKeyPressed(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                String keycode = event.getCode().toString();
                if(!level.keys.contains(keycode)) {
                    level.keys.add(keycode);
                }
                Integer presses = level.keyPresses.get(keycode);
                if(presses != null) {
                    level.keyPresses.replace(keycode, presses + 1);
                }
                else {
                    level.keyPresses.put(keycode, 1);
                }
                //System.out.println(level.keys.size());
                //System.out.println(keycode);
            }
        });
        theScene.setOnKeyReleased(new EventHandler<KeyEvent>() {
            @Override
            public void handle(KeyEvent event) {
                String keycode = event.getCode().toString();
                if(level.keys.contains(keycode)) {
                    level.keys.remove(keycode);
                }
                if(level.keyPresses.containsKey(keycode)) {
                    level.keyPresses.replace(keycode, 0);
                }
            }
        });
        //System.out.println(level.keys.size());
        gc.setFill( Color.WHITE );
        gc.setStroke( Color.WHITE );
        gc.setTextAlign(TextAlignment.CENTER);
        Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 24 );
        gc.setFont( theFont );
        System.out.println(Double.toString(gc.getLineWidth()));
        //gc.setFill( Color.RED );
        //gc.setStroke( Color.BLACK );
        //gc.setLineWidth(2);
        //Font theFont = Font.font( "Times New Roman", FontWeight.BOLD, 48 );
        //gc.setFont( theFont );
        //gc.fillText( "Hello, World!", 60, 50 );
        //gc.strokeText( "Hello, World!", 60, 50 );

        //Image earth = new Image( "earth.png" );
        //gc.drawImage( earth, 180, 100 );
        //gc.rect(0, 0, 30, 30);
        //gc.fillRect(0, 0, 10,80);
        new AnimationTimer() {
            @Override
            public void handle(long now) {
                level.update();
                level.render(gc);
            }
        }.start();
        theStage.show();
    }
}