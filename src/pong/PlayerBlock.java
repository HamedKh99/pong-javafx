package pong;

import javafx.scene.canvas.GraphicsContext;

/**
 * Created by Admin on 14/04/2017.
 */
public class PlayerBlock {
    private int xpos = 0;
    private int ypos = 0;
    public static final int BLOCK_WIDTH = 10;
    public static final int BLOCK_HEIGHT = 80;
    public static final int Y_SPEED = 4;
    public int score = 0;
    public PlayerBlock(int x, int y)
    {
        this.xpos = x;
        this.ypos = y;
    }

    public int getXpos() {
        return xpos;
    }

    public int getYpos() {
        return ypos;
    }

    public void udpate(GameLevel level) {
        int yCenter = this.getYpos() + BLOCK_HEIGHT / 2;
        double ballCenterY = level.getBall().getYpos() + GameBall.BALL_RADIUS;
        /*
        if(ballCenter < SCREEN_HEIGHT / 2) {
            return;
        }
        */
        //this.ypos = ballCenter - BLOCK_HEIGHT / 2;
        //Follow ball only if it passes the top offset
        switch (level.difficult) {
            case 0: {
                if(ballCenterY < yCenter - BLOCK_HEIGHT / 2) {
                    this.moveUp();
                }
                if(ballCenterY > yCenter + BLOCK_HEIGHT / 2) {
                    this.moveDown();
                }
            } break;
            case 1: {
                if(ballCenterY < yCenter - 10) {
                    this.moveUp();
                }
                if(ballCenterY > yCenter + 10) {
                    this.moveDown();
                }
            } break;
            case 2: {
                if(ballCenterY < yCenter) {
                    this.moveUp();
                }
                if(ballCenterY > yCenter) {
                    this.moveDown();
                }
            } break;
        }
    }
    public void render(GraphicsContext gc) {}
    public void moveUp() {
        if(this.ypos > 0) {
            this.ypos -= Y_SPEED;
        }
    }
    public void moveDown() {
        if(this.ypos < CanvasMainClass.SCREEN_HEIGHT - BLOCK_HEIGHT) {
            this.ypos += Y_SPEED;
        }
    }
}
